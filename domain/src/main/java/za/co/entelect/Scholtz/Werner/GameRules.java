package za.co.entelect.Scholtz.Werner;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static java.lang.Thread.currentThread;

public class GameRules {

    static final Logger logger = LoggerFactory.getLogger(GameRules.class);

    private List<String> winnerList = new ArrayList<>();
    private List<String> actionList = new ArrayList<>();
    private List<String> loserList = new ArrayList<>();

    public List<String> getWinnerList() {
        return winnerList;
    }

    public List<String> getActionList() {
        return actionList;
    }

    public List<String> getLoserList() {
        return loserList;
    }

    public List<String> readFiles(){
        ArrayList<String> listTemp = new ArrayList<>();
        ArrayList<String> weaponsList = new ArrayList<>();

        Scanner RulesFile = null;
        Scanner WeaponsFile = null;
        ClassLoader classLoader = currentThread().getContextClassLoader();

        try {
            logger.debug("Try reading files from Rules.txt");
            RulesFile = new Scanner(new File(classLoader.getResource("Rules.txt").getPath()));
            logger.debug("Try reading files from Weapons.txt");
            WeaponsFile = new Scanner(new File(classLoader.getResource("Weapons.txt").getPath()));
         } catch (FileNotFoundException e) {
            System.out.println("File not found. Please ensure that Rules.txt and Weapons.txt is in the resources folder.");
            logger.error("Either Rules.txt or Weapons.txt is not available to read");
            System.exit(1);
        }

        logger.debug("Writing RulesFile to listTemp");
        while (RulesFile.hasNext()) {
            String temp = RulesFile.next();
            listTemp.add(temp);
        }
        logger.debug("Writing WeaponsFile to weaponsList");
        while (WeaponsFile.hasNext()) {
            String tempWeapons = WeaponsFile.next();
            weaponsList.add(tempWeapons);
        }
        RulesFile.close();
        WeaponsFile.close();
        parseRules(listTemp);
        checkWinnerListLength(weaponsList);
        return weaponsList;
    }

    public void parseRules(ArrayList<String> listTemp) {
        logger.debug("Extracting game rules and actions into winnerList, actionList and loserList.");
        for (int ii = 0; ii < listTemp.size(); ii = ii + 3) {
            String temp2 = listTemp.get(ii);
            String temp3 = listTemp.get(ii + 1);
            String temp4 = listTemp.get(ii + 2);
            winnerList.add(temp2);
            actionList.add(temp3);
            loserList.add(temp4);
        }
    }

    public void checkWinnerListLength(ArrayList<String> weaponsList) {
        logger.debug("Check if rules list is correct size (winnersList, actionList and loserList) using the weaponsList.");
        int optionsAmount = 0;
        for (int i = 0; i < (weaponsList.size() - 1); i++) {
            optionsAmount = optionsAmount + (weaponsList.size() - 1 - i);
        }

        if (winnerList.size() == optionsAmount) {
            System.out.println(" ");
        } else if (winnerList.size() > optionsAmount) {
            System.out.println("There's too many options in the Rules List. Please look through the list and make sure there is no duplicates.");
            System.exit(1);
        } else if (winnerList.size() < optionsAmount) {
            System.out.println("There's is not enough options in the Rules List. Please look through the list and make sure there is no options left out.");
            System.exit(1);
        }
    }


}
