package za.co.entelect.Scholtz.Werner;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class AiVsAiGameType extends GameType {

    public AiVsAiGameType(List<String> weaponsList) {
        super(weaponsList);
    }

    @Override
    public void determinePlayerChoice() {
        logger.debug("Determining PC's weapon choices.");
        determinePlayerChoiceValidity(setAndGetPlayer1Choice(getWeaponsList().get(ThreadLocalRandom.current().nextInt(0, getWeaponsList().size()))));
        determinePlayerChoiceValidity(setAndGetPlayer2Choice(getWeaponsList().get(ThreadLocalRandom.current().nextInt(0, getWeaponsList().size()))));
    }

    @Override
    public void displayOutput() {
        logger.debug("Display game type and weapon options for PCvPC.");
        System.out.println(" ");
        System.out.println("********   PC vs itself. . .is this weird?   ********");
    }
}
