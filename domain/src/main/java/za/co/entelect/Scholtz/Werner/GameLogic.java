package za.co.entelect.Scholtz.Werner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.Objects;

public class GameLogic {
    static final Logger logger = LoggerFactory.getLogger(GameRules.class);

    public static void determineWinner(GameDetails details) {

        if (Objects.equals(details.getPlayer1Choice(), details.getPlayer2Choice())) {
            logger.debug("Outcome is a draw.");
            System.out.println("************************************");
            System.out.println("*****   :| It's a draw... :|   *****");
            System.out.println("************************************");
        } else {
            logger.debug("Outcome is not a draw. Must now determine winner with loop through game rules (winnerList & loserList).");
            for (int x = 0; x < details.getGameRules().getWinnerList().size(); x++) {
                if ((Objects.equals(details.getPlayer1Choice(), details.getGameRules().getWinnerList().get(x))) && (Objects.equals(details.getPlayer2Choice(), details.getGameRules().getLoserList().get(x)))) {
                    logger.debug("Found Player 1 as winner.");
                    System.out.println(details.getGameRules().getWinnerList().get(x) + " " + details.getGameRules().getActionList().get(x) + " " + details.getGameRules().getLoserList().get(x));
                    System.out.println(" ");
                    System.out.println("************************************");
                    System.out.println("*****   :D Player 1 wins! :D   *****");
                    System.out.println("************************************");
                } else if (Objects.equals(details.getPlayer2Choice(), details.getGameRules().getWinnerList().get(x)) && Objects.equals(details.getPlayer1Choice(), details.getGameRules().getLoserList().get(x))) {
                    logger.debug("Found Player 2 as winner. Display Player 1 & Player 2 choices and the applicable action.");
                    System.out.println(details.getGameRules().getWinnerList().get(x) + " " + details.getGameRules().getActionList().get(x) + " " + details.getGameRules().getLoserList().get(x));
                    System.out.println(" ");
                    System.out.println("************************************");
                    System.out.println("*****   :D Player 2 wins! :D   *****");
                    System.out.println("************************************");
                }
            }
        }


    }




}
