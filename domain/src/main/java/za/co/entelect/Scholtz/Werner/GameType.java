package za.co.entelect.Scholtz.Werner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public abstract class GameType {

    static final Logger logger = LoggerFactory.getLogger(GameType.class);

    public static GameType getGameType(int code, List<String> weaponsList) throws UndefinedChoiceException {
        if (code == 0) {
            return new ConstantInputsGameType(weaponsList);
        } else if (code == 1) {
            return new PlayerVsPlayerGameType(weaponsList);
        } else if (code == 2) {
            return new PlayerVsAiGameType(weaponsList);
        } else if (code == 3) {
            return new AiVsAiGameType(weaponsList);
        }
        throw new UndefinedChoiceException(code);
    }

    private String player1Choice;
    private String player2Choice;

    private List<String> weaponsList;

    public GameType(List<String> weaponsList) {
        player1Choice = "Spock";
        player2Choice = "Spock";
        this.weaponsList = weaponsList;
    }

    public String getPlayer1Choice() {
        return player1Choice;
    }

    public String setAndGetPlayer1Choice(String player1Choice) {
        return this.player1Choice = player1Choice;
    }

    public String getPlayer2Choice() {
        return player2Choice;
    }

    public String setAndGetPlayer2Choice(String player2Choice) {
        return this.player2Choice = player2Choice;
    }

    public List<String> getWeaponsList() {
        return weaponsList;
    }

    public abstract void determinePlayerChoice();

    public abstract void displayOutput();

    public void determinePlayerChoiceValidity(String playerChoice) {
        /*
        Should maybe change in future to not exit program
        if user spells wrong but to rather give user another try.
        */
        logger.debug("Determine player choice validity: Is it one of the options in the Weapons.txt file? Thereafter display the chosen Weapon.");
        boolean playerChoiceIsValid = false;
        if (weaponsList.contains(playerChoice)){
            System.out.println("You chose " + playerChoice + ".");
            System.out.println(" ");
            playerChoiceIsValid = true;
        }
        if (!playerChoiceIsValid) {
            System.out.println(playerChoice + " is not a valid weapon. Check your spelling. Otherwise make sure the weapon appears in the \"Weapons.txt\" file.");
            System.out.println("After the error was fixed, please start the program again.");
            logger.error("The user's choice was not valid due to either not being in Weapons.txt file or that the user spelled wrong.");
            System.exit(1);
        }
    }
}
