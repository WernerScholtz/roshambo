package za.co.entelect.Scholtz.Werner;

import java.util.List;
import java.util.Scanner;

public class PlayerVsPlayerGameType extends GameType {

    private Scanner scanner = new Scanner(System.in);

    public PlayerVsPlayerGameType(List<String> weaponsList) {
        super(weaponsList);
    }



    @Override
    public void determinePlayerChoice() {
        logger.debug("Determining Player 1's choice.");
        System.out.println("Player 1, choose your weapon:");
        determinePlayerChoiceValidity(setAndGetPlayer1Choice(scanner.nextLine()));

        logger.debug("Determining Player 2's choice.");
        System.out.println("Player 2, choose your weapon:");
        determinePlayerChoiceValidity(setAndGetPlayer2Choice(scanner.nextLine()));
    }

    @Override
    public void displayOutput() {
        logger.debug("Display game type and weapon options for PvP. Thereafter user will be asked for weapon input.");
        System.out.println(" ");
        System.out.println("********   Player vs Player   ********");
        System.out.println(" ");
        System.out.println("Your available weapons are: " + getWeaponsList() + ".");
    }
}
