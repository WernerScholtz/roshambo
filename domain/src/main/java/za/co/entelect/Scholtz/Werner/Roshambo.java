package za.co.entelect.Scholtz.Werner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Objects;
import java.util.Scanner;

public class Roshambo {

    final static Logger logger = LoggerFactory.getLogger((Roshambo.class));

    private GameRules gameRules = new GameRules();

    public GameRules getGameRules() {
        return gameRules;
    }

    public static void main(String args[]) throws IOException {

        logger.info("Starting Roshambo app");
        Roshambo roshambo = new Roshambo();
        logger.info("Creating weaponsList");
        List<String> weaponsList = roshambo.getGameRules().readFiles();
        logger.info("Created weaponsList object and getting winnerList size");
        assert roshambo.getGameRules().getWinnerList().size() != 0;

        System.out.println("*******************************************");
        System.out.println("*   Welcome to an epic game of Roshambo   *");
        System.out.println("*******************************************");

        logger.info("Staring with game play (going into while loop).");
        String quitOption = "a";
        while (!Objects.equals(quitOption, "q")) {

            System.out.println("What type of game do you want to play?");
            System.out.println("[1] - PvP, [2] - PvPC, [3] - PCvsPC");
            logger.info("Getting user input for game type");
            Scanner scanner3 = new Scanner(System.in);
            int gameType = Integer.parseInt(scanner3.nextLine());

            logger.info("Creating game type object.");
            GameType gt;
            try {
                logger.debug("Trying to get a game type from gameType.class using user input.");
                gt = GameType.getGameType(gameType, weaponsList);
            } catch (UndefinedChoiceException e) {
                System.err.println(e.getMessage());
                logger.warn("Unable to get a gameType object. Giving program a default input in order not to crash.");
                gt = new ConstantInputsGameType(weaponsList);
            }

            logger.info("Display weapon options and ask user for weapon input.");
            gt.displayOutput();
            gt.determinePlayerChoice();

            logger.info("Create details object from GameDetails.class to pass to GameLogic to determine who won.");
            GameDetails details = new GameDetails();
            details.setPlayer1Choice(gt.getPlayer1Choice());
            details.setPlayer2Choice(gt.getPlayer2Choice());
            details.setGameRules(roshambo.getGameRules());

            logger.info("Determine who won and display it.");
            GameLogic.determineWinner(details);

            logger.info("Display the end statement asking the user to play again or quit.");
            System.out.println(" ");
            System.out.println("Tell a joke and press enter to start again OR press [q] and then enter to quit.");
            Scanner scanner5 = new Scanner(System.in);
            quitOption = scanner5.nextLine();
            System.out.println(" ");
        }
    }
}