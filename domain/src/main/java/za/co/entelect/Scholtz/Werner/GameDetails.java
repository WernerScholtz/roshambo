package za.co.entelect.Scholtz.Werner;

public class GameDetails {

    private String player1Choice;
    private String player2Choice;
    private GameRules gameRules;

    public void setPlayer1Choice(String player1Choice) {
        this.player1Choice = player1Choice;
    }

    public void setPlayer2Choice(String player2Choice) {
        this.player2Choice = player2Choice;
    }

    public void setGameRules(GameRules gameRules) {
        this.gameRules = gameRules;
    }

    public String getPlayer1Choice() {
        return player1Choice;
    }

    public String getPlayer2Choice() {
        return player2Choice;
    }

    public GameRules getGameRules() {
        return gameRules;
    }
}
