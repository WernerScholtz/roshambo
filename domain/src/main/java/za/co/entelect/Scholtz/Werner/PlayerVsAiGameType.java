package za.co.entelect.Scholtz.Werner;

import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ThreadLocalRandom;

public class PlayerVsAiGameType extends GameType {

    private Scanner scanner = new Scanner(System.in);

    public PlayerVsAiGameType(List<String> weaponsList) {
        super(weaponsList);
    }

    @Override
    public void determinePlayerChoice() {
        logger.debug("Determining Player 1's choice.");
        System.out.println("Player 1, choose your weapon:");
        determinePlayerChoiceValidity(setAndGetPlayer1Choice(scanner.nextLine()));

        logger.debug("Determine PC weapon choice.");
        List<String> weaponsList = getWeaponsList();
        determinePlayerChoiceValidity(setAndGetPlayer2Choice(weaponsList.get(ThreadLocalRandom.current().nextInt(0, weaponsList.size()))));
    }

    @Override
    public void displayOutput() {
        logger.debug("Display game type and weapon options for PvPC. Thereafter user will be asked for weapon input.");
        System.out.println(" ");
        System.out.println("********   Player vs PC   ********");
        System.out.println(" ");
        System.out.println("Your available weapons are: " + getWeaponsList() + ".");
    }
}
