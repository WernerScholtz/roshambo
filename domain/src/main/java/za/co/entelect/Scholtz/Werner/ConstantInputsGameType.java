package za.co.entelect.Scholtz.Werner;

import java.util.List;

public class ConstantInputsGameType extends GameType {

    public ConstantInputsGameType(List<String> weaponsList) {
        super(weaponsList);
    }

    @Override
    public void determinePlayerChoice() {
        logger.debug("Setting Player 1 and 2 choices for debugging purposes.");
        setAndGetPlayer1Choice("Paper");
        setAndGetPlayer2Choice("Rock");
    }

    @Override
    public void displayOutput() { }
}
