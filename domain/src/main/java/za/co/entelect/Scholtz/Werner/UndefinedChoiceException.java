package za.co.entelect.Scholtz.Werner;

public class UndefinedChoiceException extends Exception {

    public UndefinedChoiceException(int code) {
        super("Invalid choice given with code: " + code);
    }
}
